<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('todos')->insert(
            [
                [
                        
                        'title' => 'Buy a Car',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        
                        'title' => 'Babyssiter',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                        
                        'title' => 'eat guyava',
                        'created_at' => date('Y-m-d G:i:s'),

                ],
                [
                        
                        'title' => 'Help Grandfather',
                        'created_at' => date('Y-m-d G:i:s'),

                ],
                [
                       
                       'title' => 'Yeladim Zigzag',
                       'created_at' => date('Y-m-d G:i:s'),
                ]
                
            
                    ]);
            
    }
}
