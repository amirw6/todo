<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return  "hello world";
});

Route::get('/student/{id?}', function ($id = 'no studemt provide') {
    return  "hello student ".$id;
})->name('students');

Route::get('/comment/{id}', function ($id) {
    return  view('comment',['id'=>$id]);
})->name('comments');


Route::get('/customers/{customerid?}', function ($customerid = 'No costumer was Provided') {
    if($customerid=='No costumer was Provided')
    return view('ncustomers', ['customerid'=>$customerid]);
    else
    return view('customers',['customerid'=>$customerid]);
    ;
})->name('customers');

Route::resource('todos','TodoController');

